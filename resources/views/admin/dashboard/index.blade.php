@extends('layouts.app')
@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">

    <style>
        .mb-20{
            margin-bottom: 20px
        }
        .datepicker{
            z-index: 9999 !important;
        }
        .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
            width: 250px !important;
        }
        .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single, .select2-search__field {
            width: 250px !important;
        }
        .select2-search__field {
            width: 150px !important;
        }
    </style>
@endpush
@section('content')
<style type="text/css">
	.flex-column.flex-2 .position.card .card-box {
    border: 1px solid #dedede !important;
    border-radius: 4px;
    margin-bottom: 15px;
}
</style>
    <div class="row">

        <div class="col-md-12">
            @if(isset($lastVersion))

                <div class="alert alert-info col-md-12">
                    <div class="col-md-10"><i class="ti-gift"></i> @lang('modules.update.newUpdate') <label class="label label-success">{{ $lastVersion }}</label>
                    </div>
                </div>
            @endif
            @if (!$user->mobile_verified && $smsSettings->nexmo_status == 'active')
                <div id="verify-mobile-info">
                    <div class="alert alert-info col-md-12" role="alert">
                        <div class="row">
                            <div class="col-md-10 d-flex align-items-center">
                                <i class="fa fa-info fa-3x mr-2"></i>
                                @lang('messages.info.verifyAlert')
                            </div>
                            <div class="col-md-2 d-flex align-items-center justify-content-end">
                                <a href="{{ route('admin.profile.index') }}" class="btn btn-warning">
                                    @lang('menu.profile')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <!-- Column -->
        {{-- <div class="col-md-6 col-lg-4 col-xlg-2">
            <div class="card">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white">{{ $totalCompanies }}</h1>
                    <h6 class="text-white">@lang('modules.dashboard.totalCompanies')</h6>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="flex-header">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown button
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
        	<div class="flex-scrollable-widget positions-content">
                
                <div class="row ht-padding">
                    <div class="flex-scroll">
                        <div class="card-box position">
                            <div class="position-info">
                                <div class="position-header">
                                    <div class="position-header-inline">
                                        <h3>
                                            <i class="icon fa fa-circle published"></i>
                                            <span class="ng-binding">{{$activeJob[0]->title}}</span>
                                        </h3>
                                        <div class="partition">
                                            <div class="row">
                                                <a class="btn btn-green" href="#" uib-tooltip="Drive More Candidates" tooltip-placement="bottom">
                                                    <i class="fa fa-bullhorn"></i>
                                                    <span>Promote</span>
                                                </a>
                                            
                                                <a class="btn btn-grey " href="{{route('admin.jobs.edit',$activeJob[0]->id)}}" uib-tooltip="Edit Position" tooltip-placement="bottom">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-body">
                                    <div class="row">
                                        <div class="col">
                                            <p>
                                                <span><i class="fa fa-map-marker"></i>
                                                    <span class="ng-binding">{{$activeJob[0]->location->location.' , '.$activeJob[0]->location->country->country_name}}</span>
                                                </span>
                                                <span class="requisition ng-scope">
                                                    <i class="fa fa-qrcode"></i>
                                                    <span class="ng-binding"> HR001</span>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="position_footer">
                                        <div class="position_footer_left_content">
                                            <a class="avatar" href="#">a</a>
                                            <span class="avatar">+</span>
                                        </div>
                                        <div class="position_footer_right_content">
                                            <a href="#">Pipeline</a>
                                            <a href="#">Candidates</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4" id="right-content">
            <div class="row ht-padding">
                <div class="card" style="background: none;width: 100%;">
                    <div class="card-body">
                        <div class="card-title">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-users"></i> Dropdown button
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute;will-change: transform;top: 0px;left: 0px;transform: translate3d(0px, 64px, 0px);">
                                    <a class="dropdown-item" href="#">New Candidates</a>
                                    <a class="dropdown-item" href="#">My Candidates</a>
                                    <a class="dropdown-item" href="#">Unseen Candidates</a>
                                </div>
                            </div>
                        </div>
                        @if($allApplications->count() > 0)
                            @foreach($allApplications as $application)
                                @if($application->is_viewed == 0)
                                    <div class="flex-scroll" style="display: inline-flex;">
                                        <a class="avatar align-middle show-detail" href="javascript:;" data-widget="control-sidebar" data-slide="true" data-row-id="{{$application->id}}}">
                                            {{ str_limit($application->full_name, $limit = 1, $end ='') }}
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{$application->full_name}}</li>
                                            <li class="list-group-item">
                                                <span class="ng-binding"><i class="fa fa-briefcase"></i>Sourcing Manager</span>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="flex-scroll" style="text-align: center;">
                                <h5><i class="fa fa-users"></i></h5>
                                <p>No upcoming meetings</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row ht-padding">
                <div class="card" style="background: none;width: 100%;">
                    <div class="card-body">
                        <div class="card-title">
                            <i class="fa fa-calendar"></i> My Agenda
                        </div>
                        <div class="flex-scroll" style="text-align: center;">
                            <h5><i class="fa fa-calendar"></i></h5>
                            <p>No upcoming meetings</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ht-padding">
                <div class="card" style="background: none;width: 100%;">
                    <div class="card-body">
                        <div class="card-title">
                            <i class="fa fa-calendar"></i> My Task
                        </div>
                        <div class="flex-scroll" style="text-align: center;">
                            <h5><i class="fa check-square"></i></h5>
                            <p>No upcoming meetings</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jQueryUI/jquery-ui.min.js') }}"></script>

    <script>
        var updated = true;

        function showNewTodoForm() {
            let url = "{{ route('admin.todo-items.create') }}"

            $.ajaxModal('#application-modal', url);
        }

        function initSortable() {
            let updates = {'pending-tasks': false, 'completed-tasks': false};
            let completedFirstPosition = $('#completed-tasks').find('li.draggable').first().data('position');
            let pendingFirstPosition = $('#pending-tasks').find('li.draggable').first().data('position');

            $('#pending-tasks').sortable({
                connectWith: '#completed-tasks',
                cursor: 'move',
                handle: '.handle',
                stop: function (event, ui) {
                    const id = ui.item.data('id');
                    const oldPosition = ui.item.data('position');

                    if (updates['pending-tasks']===true && updates['completed-tasks']===true)
                    {
                        const inverseIndex =  completedFirstPosition > 0 ? completedFirstPosition - ui.item.index() + 1 : 1;
                        const newPosition = inverseIndex;

                        updateTodoItem(id, position={oldPosition, newPosition}, status='completed');

                    }
                    else if(updates['pending-tasks']===true && updates['completed-tasks']===false)
                    {
                        const newPosition = pendingFirstPosition - ui.item.index();

                        updateTodoItem(id, position={oldPosition, newPosition});
                    }

                    //finally, clear out the updates object
                    updates['pending-tasks']=false;
                    updates['completed-tasks']=false;
                },
                update: function (event, ui) {
                    updates[$(this).attr('id')] = true;
                }
            }).disableSelection();

            $('#completed-tasks').sortable({
                connectWith: '#pending-tasks',
                cursor: 'move',
                handle: '.handle',
                stop: function (event, ui) {
                    const id = ui.item.data('id');
                    const oldPosition = ui.item.data('position');

                    if (updates['pending-tasks']===true && updates['completed-tasks']===true)
                    {
                        const inverseIndex =  pendingFirstPosition > 0 ? pendingFirstPosition - ui.item.index() + 1 : 1;
                        const newPosition = inverseIndex;

                        updateTodoItem(id, position={oldPosition, newPosition}, status='pending');
                    }
                    else if(updates['pending-tasks']===false && updates['completed-tasks']===true)
                    {
                        const newPosition = completedFirstPosition - ui.item.index();

                        updateTodoItem(id, position={oldPosition, newPosition});
                    }

                    //finally, clear out the updates object
                    updates['pending-tasks']=false;
                    updates['completed-tasks']=false;
                },
                update: function (event, ui) {
                    updates[$(this).attr('id')] = true;
                }
            }).disableSelection();
        }
        function updateTodoItem(id, pos, status=null) {
            let data = {
                _token: '{{ csrf_token() }}',
                id: id,
                position: pos,
            };

            if (status) {
                data = {...data, status: status}
            }

            $.easyAjax({
                url: "{{ route('admin.todo-items.updateTodoItem') }}",
                type: 'POST',
                data: data,
                container: '#todo-items-list',
                success: function (response) {
                    $('#todo-items-list').html(response.view);
                    initSortable();
                }
            });
        }

        function showUpdateTodoForm(id) {
            let url = "{{ route('admin.todo-items.edit', ':id') }}"
            url = url.replace(':id', id);

            $.ajaxModal('#application-modal', url);
        }

        function deleteTodoItem(id) {
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    let url = "{{ route('admin.todo-items.destroy', ':id') }}";
                    url = url.replace(':id', id);

                    let data = {
                        _token: '{{ csrf_token() }}',
                        _method: 'DELETE'
                    }

                    $.easyAjax({
                        url,
                        data,
                        type: 'POST',
                        container: '#roleMemberTable',
                        success: function (response) {
                            if (response.status == 'success') {
                                $('#todo-items-list').html(response.view);
                                initSortable();
                            }
                        }
                    })
                }
            });
        }

        @if ($user->roles->count() > 0)
            $('#todo-items-list').html(`{!! $todoItemsView !!}`);
        @endif

        initSortable();

        $('body').on('click', '#create-todo-item', function () {
            $.easyAjax({
                url: "{{route('admin.todo-items.store')}}",
                container: '#createTodoItem',
                type: "POST",
                data: $('#createTodoItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        $('#todo-items-list').html(response.view);
                        initSortable();

                        $('#application-modal').modal('hide');
                    }
                }
            })
        });

        $('body').on('click', '#update-todo-item', function () {
            const id = $(this).data('id');
            let url = "{{route('admin.todo-items.update', ':id')}}"
            url = url.replace(':id', id);

            $.easyAjax({
                url: url,
                container: '#editTodoItem',
                type: "POST",
                data: $('#editTodoItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        $('#todo-items-list').html(response.view);
                        initSortable();

                        $('#application-modal').modal('hide');
                    }
                }
            })
        });

        $('body').on('change', '#todo-items-list input[name="status"]', function () {
            const id = $(this).data('id');
            let status = 'pending';

            if ($(this).is(':checked')) {
                status = 'completed';
            }

            updateTodoItem(id, null, status);
        })

        $('.show-detail').on('click', function () {
            $(".right-sidebar").slideDown(50).addClass("shw-rside");

            var id = $(this).data('row-id');
            var url = "{{ route('admin.job-applications.show',':id') }}";
            url = url.replace(':id', id);

            $.ajax({
                type: 'GET',
                url: url,
                success: function (response) {
                    if (response.status == "success") {
                        $('#right-sidebar-content').html(response.view);
                    }
                }
            });
        });
    </script>
@endpush