<div class="row">
    <div class="col-md-12">
        <h4 class="box-title" id="structure">Update Log</h4>
        <pre>
   <p>
        ├──
        │
        │   └── <strong class="font-bold">Version 3.3.0</strong>
        │       └── New Feature: Candidate database for archived candidates
        │       └── New Feature: To-do list
        │       └── New Feature: Super admin can Login as company admin
        │       └── Bug fixes
        │
        │   └── <strong class="font-bold">Version 3.2.0</strong>
        │       └── New Feature: Job Onboarding
        │       └── Bug fixes
        │
        │   └── <strong class="font-bold">Version 3.1.2</strong>
        │       └── Razorpay Payment Gateway Integration
        │       └── Bug fixes
        │
        │   └── <strong class="font-bold">Version 3.1.1</strong>
        │       └── Bug fixes
        │
        │   └── <strong class="font-bold">Version 3.1.0</strong>
        │       └── Send Emails to applicants
        │       └── Nexmo SMS Feature
        │
        │   └── <strong class="font-bold">Version 3.0.8</strong>
        │       └── Stripe Setting added
        │
        │   └── <strong class="font-bold">Version 3.0.6</strong>
        │       └── Bug fixes
        │
        │   └── <strong class="font-bold">Version 3.0.2</strong>
        │       └── Auto upgrade bug fixes
        │
        │   └── <strong class="font-bold">Version 3.0.0</strong>
        │       └── Laravel 5.8 Upgrade
        │       └── Bug Fixes
        │
        │   └── <strong class="font-bold">Version 2.0.3</strong>
        │       └── Added Sign in via linkedin for candidates. Superadmin setup required.
        │       └── Add applicant notes.
        │       └── Added email notification for candidate after applying and application rejected.
        │       └── Added job filter on job application board.
        │       └── Bug fixes.
        │
        │   └── <strong class="font-bold">Version 2.0.2</strong>
        │       └── Bug fixes.
        │
        │   └── <strong class="font-bold">Version 2.0.1</strong>
        │       └── Verification email on register.
        │
        │   └── <strong class="font-bold">Version 2.0</strong>
        │       └── Saas Version.
        │
        │   └── <strong class="font-bold">Version 1.4.4</strong>
        │       └── Bug fixes.
        │
        │   └── <strong class="font-bold">Version 1.4.3</strong>
        │       └── Added companies modules to manage jobs for multiple companies.
        │
        │   └── <strong class="font-bold">Version 1.3</strong>
        │       └── Added interview schedule feature.
        │
        │   └── <strong class="font-bold">Version 1.2</strong>
        │       └── Added email settings in settings section.
        │       └── Added custom job questions feature for job posts.
        │       └── Added export to excel button for job applications.
        │       └── Added filter for job applications table view.
        │
        │   └── <strong class="font-bold">Version 1.1</strong>
        │       └── Theme changed due to copyright issues. No major chnages in the UI.
        │
        │   └── <strong class="font-bold">Version 1.0</strong>
        │       └── Initial Release
        │
        └──
    </p>
                                        </pre>
    </div>
</div>
