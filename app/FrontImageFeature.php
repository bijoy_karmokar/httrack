<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontImageFeature extends Model
{
    protected $appends = [
        'image_url'
    ];

    public function getImageUrlAttribute()
    {
        return asset_url('front-features/' . $this->image);
    }

}
