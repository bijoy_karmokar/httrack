<?php

namespace App\Http\Controllers\SuperAdmin;

class UpdateApplicationController extends SuperAdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('menu.updateApplication');
        $this->pageIcon = __('ti-settings');
    }

    public function index()
    {

        return view('super-admin.update-application.index', $this->data);
    }

}
