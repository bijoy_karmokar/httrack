<?php

namespace App\Http\Controllers\SuperAdmin;

use App\FrontCmsHeader;
use App\Helper\Files;
use Illuminate\Http\Request;
use App\Helper\Reply;
use App\FrontImageFeature;
use App\Http\Requests\SuperAdmin\StoreImageFeature;
use App\Http\Requests\SuperAdmin\UpdateImageFeature;

class SuperAdminFrontCmsController extends SuperAdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageIcon = 'icon-screen-desktop';
        $this->pageTitle = __('menu.frontCms');
    }

    public function index()
    {
        $this->headerData = FrontCmsHeader::first();
        return view('super-admin.front-cms.index', $this->data);
    }

    public function updateHeader(Request $request)
    {
        $data = $request->all();
        $headerData = FrontCmsHeader::first();

        if ($request->hasFile('logo')) {
            $data['logo'] = Files::upload($request->logo,'front-logo');
        }

        if ($request->hasFile('header_image')) {
            $data['header_image'] = Files::upload($request->header_image,'header-image');
        }

        if ($request->hasFile('header_backround_image')) {
            $data['header_backround_image'] = Files::upload($request->header_backround_image,'header-background-image');
        }

        if ($request->remove_header_background == 'yes') {
            $data['header_backround_image'] = null;
        }

        $headerData->update($data);


        return Reply::redirect(route('superadmin.front-cms.index'), __('menu.settings') . ' ' . __('messages.updatedSuccessfully'));
    }

    public function imageFeatures()
    {
        $this->features = FrontImageFeature::all();
        return view('super-admin.front-cms.features', $this->data);
    }

    public function saveImageFeatures(StoreImageFeature $request)
    {
        $headerData = new FrontImageFeature();
        $headerData->title = $request->title;
        $headerData->description = $request->description;

        if ($request->hasFile('image')) {
            $headerData->image = Files::upload($request->image,'front-features');
        }

        $headerData->save();

        return Reply::redirect(route('superadmin.front-cms.features'), __('menu.settings') . ' ' . __('messages.updatedSuccessfully'));
    }

    public function updatefeatures(UpdateImageFeature $request, $id)
    {
        $headerData = FrontImageFeature::findOrFail($id);
        $headerData->title = $request->title;
        $headerData->description = $request->description;

        if ($request->hasFile('image')) {
            $headerData->image = Files::upload($request->image,'front-features');
        }

        $headerData->save();

        return Reply::redirect(route('superadmin.front-cms.features'), __('menu.settings') . ' ' . __('messages.updatedSuccessfully'));
    }

    public function editImageFeatures($id)
    {
        $this->feature = FrontImageFeature::findorFail($id);
        return view('super-admin.front-cms.edit_feature', $this->data);
    }

    public function deleteFeature(Request $request, $id)
    {
        $feature = FrontImageFeature::findorFail($id);
        Files::deleteFile($feature->image, 'front-features');
        $feature->delete();
        return Reply::success(__('messages.recordDeleted'));
    }

}
