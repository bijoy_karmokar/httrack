<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\ThemeSetting;
use Froiden\Envato\Traits\AppBoot;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\GlobalSetting;
use Illuminate\Support\Facades\App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, AppBoot;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');

        App::setLocale($this->global->locale);
    }

    public function showLoginForm()
    {
        if (!$this->isLegal()) {
            return redirect('verify-purchase');
        }

        if(auth()->check()) {
            if(auth()->user()->is_superadmin) {
                return redirect('super-admin/dashboard');
            }
            return redirect('admin/dashboard');
        }

        $setting = $this->global;
        $frontTheme = ThemeSetting::whereNull('company_id')->first();
        return view('auth.login', compact('setting', 'frontTheme'));
    }

    protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return [
            'email' => $request->{$this->username()},
            'password' => $request->password,
            'status' => 'active'
        ];
    }

    protected function redirectTo()
    {
        $user = auth()->user();
        if($user->is_superadmin) {
            return 'super-admin/dashboard';
        }
        return 'admin/dashboard';
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
    }
}
