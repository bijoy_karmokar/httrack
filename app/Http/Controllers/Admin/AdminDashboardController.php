<?php

namespace App\Http\Controllers\Admin;

use App\InterviewSchedule;
use App\Job;
use App\JobApplication;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Company;
use App\User;
use App\TodoItem;
class AdminDashboardController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageIcon = 'icon-speedometer';
    }

    public function index()
    {
        $lang_code = \Cookie::get('language_code');
        if ($lang_code === 'ru') {
            $this->pageTitle = __('Приборная панель');
        } elseif ($lang_code === 'fr') {
            $this->pageTitle = __('Tableau de bord');
        } elseif ($lang_code === 'es') {
            $this->pageTitle = __('Tablero');
        } else {
            $this->pageTitle = __('Dashboard');
        }
        $this->totalOpenings = Job::activeJobsCount();
        
        $allApplications = JobApplication::all();
        $toItem = TodoItem::where('status','=','pending')->get();
        $InterviewSchedule = InterviewSchedule::orderBy('id','desc')->skip(0)->take(5)->get();
        $activeJob = Job::frontActiveJobs(user()->company_id);
        //dd($activeJob->location());
        $this->totalApplications = count($allApplications);

        $this->totalHired = $allApplications->filter(function ($value, $key) {
            return $value->status  == 'hired';
        })->count();

        $this->totalRejected = $allApplications->filter(function ($value, $key) {
            return $value->status  == 'rejected';
        })->count();

        $this->newApplications = $allApplications->filter(function ($value, $key) {
            return $value->status  == 'applied';
        })->count();

        $this->shortlisted = $allApplications->filter(function ($value, $key) {
            return $value->status  == 'phone screen' || $value->status == 'interview';
        })->count();
        

        $currentDate = Carbon::now()->format('Y-m-d');

        $this->totalTodayInterview = InterviewSchedule::where(DB::raw('DATE(`schedule_date`)'),  "$currentDate")
            ->count();
        $this->todoItemsView = $this->generateTodoView();
        //dd($this->data);
        return view('admin.dashboard.index', $this->data)->with('allApplications', $allApplications)->with ('todoItem', $toItem)->with('InterviewSchedule',$InterviewSchedule)->with('activeJob',$activeJob);
        
    }
}
