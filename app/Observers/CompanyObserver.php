<?php

namespace App\Observers;

use App\Company;
use App\Helper\Files;
use App\Role;
use Illuminate\Support\Facades\DB;
use App\Package;
use App\CompanyPackage;
use Carbon\Carbon;
use App\ThemeSetting;
use App\Permission;

class CompanyObserver
{
    public function updating(Company $company){
        $original = $company->getOriginal();
        if ($company->isDirty('logo')){
            Files::deleteFile($original['logo'], 'company-logo');
        }
    }
    public function created(Company $company)
    {
        if ($company->id > 1) {

            // add company default role
            $roleUser = new Role();
            $roleUser->name = 'admin';
            $roleUser->display_name = 'Administrator';
            $roleUser->description = 'Admin is allowed to manage everything of the app.';
            $roleUser->company_id = $company->id;
            $roleUser->save();

            $permissions = Permission::all();
            foreach ($permissions as $permission) {
                $create = Permission::find($permission->id);
                $roleUser->attachPermission($create);
            }

            //add company default job application status
            $data = [
                ['status' => 'applied', 'company_id' => $company->id],
                ['status' => 'phone screen', 'company_id' => $company->id],
                ['status' => 'interview', 'company_id' => $company->id],
                ['status' => 'hired', 'company_id' => $company->id],
                ['status' => 'rejected', 'company_id' => $company->id]
            ];
            DB::table('application_status')->insert($data);

            //add default theme settings
            $theme = new ThemeSetting();
            $theme->primary_color = '#1579d0';
            $theme->company_id = $company->id;
            $theme->admin_custom_css = '/*Enter your custom css after this line*/ 
            .sidebar-dark-primary {
            background-image: linear-gradient(to top, #00c6fb 0%, #005bea 100%);
            }';
            $theme->save();

            //assign trial package
            $trialPackage = Package::where('is_trial', 1)->first();
            $checkTrialPackage = CompanyPackage::where('company_id', $company->id)
                ->where('package_id', $trialPackage->id)->first();

            if (!is_null($trialPackage) && is_null($checkTrialPackage)) {
                $companyPackage = new CompanyPackage();
                $companyPackage->company_id = $company->id;
                $companyPackage->package_id = $trialPackage->id;
                $companyPackage->status = 'active';
                $companyPackage->start_date = Carbon::today()->format('Y-m-d');
                $companyPackage->end_date = Carbon::today()->addDays($trialPackage->trial_duration)->format('Y-m-d');
                $companyPackage->save();

                Company::where('id', $company->id)->update(['package_id' => $trialPackage->id]);
            }
        }
    }

}
