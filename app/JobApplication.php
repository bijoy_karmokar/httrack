<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobApplication extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['dob'];

    protected $casts = [
        'skills' => 'array'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check()) {
                $builder->where('job_applications.company_id', user()->company_id);
            }
        });
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function onboard()
    {
        return $this->hasOne(Onboard::class, 'job_application_id');
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class);
    }

    public function status()
    {
        return $this->belongsTo(ApplicationStatus::class, 'status_id');
    }

    public function schedule()
    {
        return $this->hasOne(InterviewSchedule::class)->latest();
    }

    public function notes()
    {
        return $this->hasMany(ApplicantNote::class, 'job_application_id')->orderBy('id', 'desc');
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }

}
